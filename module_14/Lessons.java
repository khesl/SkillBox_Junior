package module_14;

public class Lessons {

    public static void main(String[] args) {

    }

    private static void lesson_1() {
        // Урок 1. Структура веб­страницы
        //
        // Найти сайт, страницы которого размечены в соответствии со стандартом HTML5 и содержат все или
        // большинство элементов структуры внутри “body” (“header”, “footer”, “nav”, “section”, “article” и “aside”).

        /**
           Например сайт https://onlymega.com/#/ имеет структуру:
         <html>
            <head></head>
            <body>
                <header>...</header>

                <section>...</section>
                <section>...</section>

                <footer>...</footer>
            </body>
         </html>

         * */
    }

    private static void lesson_3() {
        // Урок 3. Блочные HTML­элементы
        //
        // Создать веб­страницу и написать в ней примеры использования следующих тэгов:
        // a, b, audio, br, code, div, h1­h6, hr, iframe, img, p, списки (ol, ul, li), table, th, tr, td, pre, video.
        System.out.println("Realized.");
    }

    private static void lesson_4() {
//         Урок 4. Каскадные таблицы стилей (CSS)
//
//         Создать веб­страницу и написать в ней примеры использования следующих стилей: все стили для анимации,
//         @media­запросы (для нескольких разрешений экранов ­ настольный компьютер, планшет, смартфон),
//         border со всеми параметрами, cursor, margin, padding, position (+top, left, right, bottom и z­index),
//         все псевдоклассы ссылок, width, height, max­height, max­width, min­height, min- width, border­radius,
//         list­style, border­collapse, text­align, line­height, text­indent, display, float, overflow,
//         vertical­align, background, color, opacity, все стили для шрифтов.
    }

    private static void lesson_5() {
        // Урок 5. Подключение JavaScript­-кода
        //
        // Ознакомиться с документацией по jQuery. Попробовать что­нибудь из jQueryUI.
    }

    private static void lesson_6() {
        // Урок 6. Java Server Pages (JSP)
        //
        // В предыдущем модуле (оптимизация по памяти) анализировался файл с избирателями (проект VoteAnalyzer).
        // Нужно реализовать анализ файла (самого маленького “data­0.2М.xml”) в веб-­приложении и вывести результат
        // анализа в виде таблицы таким образом, чтобы в строках находились избирательные участки, в столбцах – дни,
        // а в ячейках – диапазоны времени. Файл с данными может лежать где угодно.
    }

}
