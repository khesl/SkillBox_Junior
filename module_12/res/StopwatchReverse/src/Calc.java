package module_12.res.StopwatchReverse.src;

import module_12.res.StopwatchReverse.src.StopwatchForm;

import javax.swing.*;

public class Calc extends Thread {



    private JLabel jLabel;
    private JSpinner spinnerMSEC;
    private JSpinner spinnerSEC;
    private JSpinner spinnerMIN;
    private JSpinner spinnerH;
    private JPanel rootPanel;


    public Calc(StopwatchForm form){

        this.rootPanel = form.getRootPanel();
        this.jLabel = form.getSecLabel();
        this.spinnerMSEC = form.getSpinnerMSEC();
        this.spinnerSEC = form.getSpinnerSEC();
        this.spinnerMIN = form.getSpinnerMIN();
        this.spinnerH = form.getSpinnerH();

    }
    @Override
    public void run()  {

        super.run();

        int msec = (int) spinnerMSEC.getValue();
        int sec = (int) spinnerSEC.getValue();
        int min = (int) spinnerMIN.getValue();
        int hour = (int) spinnerH.getValue();






        for (int i=msec; (i > 0 || sec > 0 || min > 0 || hour > 0)  ; i--) {

            if (i == 0 && sec != 0) {
                i=999;sec--; }

                else if (sec == 0 && min != 0 ) {
                    sec=59;min--; }

                    else if (min == 0 && hour !=0) {
                        min=59;hour--; }

            jLabel.setText(String.valueOf(hour) + ":" + String.valueOf(min) + ":" + String.valueOf(sec) + ":" +String.valueOf(i));

            try {
                sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Loader.frameNotVisible();
        JOptionPane.showMessageDialog(rootPanel, "Time is up","StopwatchReverse",JOptionPane.PLAIN_MESSAGE);
        System.exit(0);





    }
}
