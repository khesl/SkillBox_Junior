package module_12.res.StopwatchReverse.src;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;import java.awt.event.ComponentAdapter;

public class StopwatchForm {

    private JPanel rootPanel;
    private JButton startStopButton;
    private JButton pauseContButton;
    private javax.swing.JLabel SecLabel;
    private JPanel panel1;
    private JSpinner spinnerMSEC;
    private JSpinner spinnerSEC;
    private JSpinner spinnerH;
    private JSpinner spinnerMIN;
    public Calc calculator;

    private String stoper = "Stop!";
    private String starter = "Start!";
    private String pauser = "Pause!";
    private String cont = "Continue";



    public StopwatchForm()  {

        SpinnerNumberModel spinnerNumberModelMSEC = new SpinnerNumberModel(0, 0, 999, 1);
        SpinnerNumberModel spinnerNumberModelSEC = new SpinnerNumberModel(0,0,59,1);
        SpinnerNumberModel spinnerNumberModelMIN = new SpinnerNumberModel(0,0,59,1);
        SpinnerNumberModel spinnerNumberModelH = new SpinnerNumberModel(0,0,24,1);

        spinnerMSEC.setModel(spinnerNumberModelMSEC);
        spinnerSEC.setModel(spinnerNumberModelSEC );
        spinnerMIN.setModel(spinnerNumberModelMIN);
        spinnerH.setModel(spinnerNumberModelH);

        startStopButton.setText(starter);
        pauseContButton.setText(pauser);
        pauseContButton.setEnabled(false);
        StopwatchForm form = this;

        startStopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {


                if (startStopButton.getText().equals(starter)) {

                    calculator = new Calc(form);
                    calculator.start();
                    startStopButton.setText(stoper);
                    pauseContButton.setEnabled(true);

                } else if (startStopButton.getText().equals(stoper)){
                    calculator.stop();
                    SecLabel.setText("0:0:0:0");
                    startStopButton.setText(starter);
                    pauseContButton.setEnabled(false);
                    pauseContButton.setText(pauser);
                }
            }
        });
        pauseContButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (pauseContButton.getText().equals(pauser)){
                    calculator.suspend();
                    pauseContButton.setText(cont);
                } else if (pauseContButton.getText().equals(cont)){
                    calculator.resume();
                    pauseContButton.setText(pauser);
                }
            }
        });
    spinnerH.addComponentListener(new ComponentAdapter() { } );}

    public JButton getStartStopButton() {
        return startStopButton;
    }

    public void setStartStopButton(JButton startStopButton) {
        this.startStopButton = startStopButton;
    }

    public JButton getPauseContButton() {
        return pauseContButton;
    }

    public void setPauseContButton(JButton pauseContButton) {
        this.pauseContButton = pauseContButton;
    }

    public JLabel getSecLabel() {
        return SecLabel;
    }

    public void setSecLabel(JLabel secLabel) {
        SecLabel = secLabel;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public JSpinner getSpinnerMSEC() {
        return spinnerMSEC;
    }

    public void setSpinnerMSEC(JSpinner spinnerMSEC) {
        this.spinnerMSEC = spinnerMSEC;
    }

    public JSpinner getSpinnerSEC() {
        return spinnerSEC;
    }

    public void setSpinnerSEC(JSpinner spinnerSEC) {
        this.spinnerSEC = spinnerSEC;
    }

    public JSpinner getSpinnerH() {
        return spinnerH;
    }

    public void setSpinnerH(JSpinner spinnerH) {
        this.spinnerH = spinnerH;
    }

    public JSpinner getSpinnerMIN() {
        return spinnerMIN;
    }

    public void setSpinnerMIN(JSpinner spinnerMIN) {
        this.spinnerMIN = spinnerMIN;
    }

}
