package module_12.res.StopwatchReverse.src;

import javax.swing.*;

public class Loader {
    private static JFrame frame;




    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                frame = new JFrame();

                StopwatchForm form = new StopwatchForm();
                frame.setContentPane(form.getRootPanel());

                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setTitle("StopwatchReverse");

                frame.setSize(300, 170);
                frame.setResizable(false);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    public static void frameNotVisible() {

        frame.setVisible(false);
    }
}
