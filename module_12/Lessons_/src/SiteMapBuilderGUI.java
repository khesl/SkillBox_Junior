package module_12.Lessons_.src;

import Utils.ConsoleColor;
import module_12.Lessons;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class SiteMapBuilderGUI {
    private static Lessons main = null;
    private JPanel rootPanel;
    private JPanel centerPanel;
    private JPanel topPanel;
    private JTextField SearchTextField;
    private JButton startButton;
    private JButton stopButton;
    private JButton fileToSaveButton;
    private JTextField chooseFileToSaveTextField;
    private JTextArea ConsoleTextArea;
    private JTextArea StatusTextArea;
    private JPanel ThreadsPanel;
    private JComboBox threadCountComboBox;
    private JTextField setTimerText;
    private JButton timerButton;
    private JLabel timerLabel;

    private MyHttpScannerController mhsc;
    private String searchPath;
    private boolean useTimer = false;

    private TimerClass timerClass;

    public TimerClass getTimerClass(){ return timerClass; }

    private void resetMainObjects() {
        timerClass.interrupt();
        startButton.setText("start");
        startButton.setEnabled(true);
        threadCountComboBox.setEnabled(true);
        timerClass = new TimerClass(timerLabel);
        StatusTextArea.setText("");
        mhsc = null;
    }

    public SiteMapBuilderGUI(Lessons main){
        this.main = main;
        timerClass = new TimerClass(timerLabel);

        // start button
        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    super.mouseClicked(e);
                    if (MyHttpScannerController.isPause()) {
                        MyHttpScannerController.setPause(false);
                        return;
                    }
                    if (SearchTextField.getText().length() == 0) return;
                    searchPath = SearchTextField.getText();
                    mhsc = new MyHttpScannerController(new TreeLinks(0, searchPath, searchPath), timerClass);
                    mhsc.setThreads_count(Integer.valueOf(threadCountComboBox.getModel().getSelectedItem().toString()));
                    threadCountComboBox.setEditable(false);

                    ThreadUpdater updater = new ThreadUpdater();
                    if (isUseTimer()) timerClass.start();
                    if (!timerClass.isStarted()) {
                        timerClass.setTimerTime(Long.valueOf(setTimerText.getText()));
                        timerClass.startStopWatch();
                    }
                    mhsc.start();
                    updater.start();
                    ConsoleTextArea.setText("\tWork in process..");
                    startButton.setEnabled(false); // нужно пофиксить обнуление всего, иначе кидает эррор
                    stopButton.setEnabled(true);
                    threadCountComboBox.setEnabled(false);
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(null, "Please restart application!", "Restart page", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        stopButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                // кнопка паузы
                mhsc.getLoader().setPause(!mhsc.getLoader().isPause());
                mhsc.setPause(true);
                if (timerClass.isPause()){
                    // сейчас на паузе
                    timerClass.pause();
                    stopButton.setText("pause");
                } else {
                    // должно идти время
                    timerClass.pause();
                    stopButton.setText("play");
                }
                if (MyHttpScannerController.isPause()) {
                    if (stopButton.getText().equals("stop")) mhsc.setStop(true);
                    stopButton.setText("stop");
                }
            }
        });
        fileToSaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fileopen = new JFileChooser();
                fileopen.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                int ret = fileopen.showDialog(null, "Выбрать файл");
                System.out.println(fileopen.getSelectedFile());
                File chooseFile = fileopen.getSelectedFile();

            }
        });

        timerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isUseTimer()){
                    // тайер не был включен
                    setUseTimer(true);
                    ((JButton) e.getSource()).setText("Turn off");
                } else {
                    // таймер был включен
                    setUseTimer(false);
                    ((JButton) e.getSource()).setText("Turn  on");
                }
                setTimerText.setEnabled(isUseTimer());
                timerLabel.setEnabled(isUseTimer());
            }
        });
    }

    public class ThreadUpdater extends Thread {

        @Override
        public void run() {
            while (!interrupted()) {
                if (mhsc != null) {
                    if (!mhsc.getLoader().isPause()) {
                        //System.out.println("update visio.");
                        StringBuilder status = new StringBuilder();
                        for (MyHttpScanner_2 scanner : mhsc.getScanners()) {
                            status.append(scanner.getStatus()).append("\n");
                        }
                        StatusTextArea.setText(ConsoleColor.clearColor(status.toString()));

                        if (mhsc.isUpdateLinks()) {
                            ConsoleTextArea.setText(mhsc.getUniqueLinks().get(searchPath).toAllString());
                        }
                    }
                }
                if (mhsc != null) {
                    TreeLinks rootChild;
                    if ((rootChild = mhsc.getRootChild()) != null ) {
                        ConsoleTextArea.setText(ConsoleColor.clearColor(rootChild.toAllString()).replaceAll("\t", "        "));
                    } else {
                        ConsoleTextArea.append(".");
                    }
                    if (!mhsc.inProcess()) {
                        JOptionPane.showMessageDialog(null, "Work finished.", "Success", JOptionPane.PLAIN_MESSAGE);
                        resetMainObjects();
                        break;
                    }
                }
                try {
                    sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeToFile(String path, String text){}


    public boolean isUseTimer() { return useTimer; }
    public void setUseTimer(boolean useTimer) { this.useTimer = useTimer; }

    public JPanel getRootPanel(){ return rootPanel; }
}
