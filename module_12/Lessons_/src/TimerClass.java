package module_12.Lessons_.src;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimerClass extends Thread {
    private static String DEFAULT_FORMAT = "HH:mm:ss.S";
    private Long startTime;
    private Long stopWatchTime = 0L;
    private volatile boolean pause = true;
    private boolean started = false;
    private JLabel stopWatchTimeLabel;
    private JPanel clockPanel;
    private boolean useClockPanel = false;
    private Long updateTime;
    private Long timerTime;
    private boolean setTimerTime = false;
    private boolean inWork;

    public TimerClass(JLabel stopWatchTimeLabel){
        this.stopWatchTimeLabel = stopWatchTimeLabel;
        startTime = System.currentTimeMillis();
        updateTime = System.currentTimeMillis();
    }
    public TimerClass(JLabel stopWatchTimeLabel, JPanel clockPanel){
        this(stopWatchTimeLabel);
        this.clockPanel = clockPanel;
    }

    public void setTimerTime(Long timerTime){
        this.timerTime = getSecToLong(timerTime);
        System.out.println("time: " + timerTime);
        setTimerTime = true;
    }

    @Override
    public void run(){
        if (!isSetTimerTime()) throw new NullPointerException("TimerTime not Set!");
        super.run();
        while (true){
            if (!isInterrupted()){
                if (!isPause()){
                    if (System.currentTimeMillis() < (startTime + timerTime)) {
                        stopWatchTime = timerTime - (System.currentTimeMillis() - startTime);
                        stopWatchTimeLabel.setText(getFormattedTime(new SimpleDateFormat(DEFAULT_FORMAT), stopWatchTime));
                        if (isUseClockPanel()) clockPanel.repaint();
                        inWork = true;
                    } else {
                        stopWatchTimeLabel.setText(getFormattedTime(new SimpleDateFormat(DEFAULT_FORMAT), 0L));
                        if (isUseClockPanel()) clockPanel.repaint();
                        inWork = false;
                    }
                }
            }
        }

    }

//    public void updateClock(){
//        if (clockPanel != null){
//            updateTime = System.currentTimeMillis();
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(new Date(stopWatchTime));
//            calendar.setTimeZone(TimeZone.getTimeZone("GMT0"));
//            double angle = Math.toRadians(minutes*(360/60));
//            int minutes = Integer.parseInt(getFormattedTime(new SimpleDateFormat("ss")));
//            System.out.println (minutes);
//            System.out.print(angle + ": ");
//            Point center = new Point(clockPanel.getWidth()/2, clockPanel.getHeight()/2);
//            double radius = 0.85*(clockPanel.getWidth()/2);
//            Point newPoint = new Point((int) (radius * Math.sin(angle)), (int) (radius * Math.cos(angle)));
//            System.out.println(center.x + " " + center.y + " " + (center.x - newPoint.x) + " " + (center.y - newPoint.y));
//
//            clockPanel.getGraphics().drawLine(center.x, center.y, center.x + newPoint.x,center.y - newPoint.y);
//
//            clockPanel.repaint();
//        }
//    }

    public void startStopWatch(){
        started = true;
        setPause(false);
        startTime = System.currentTimeMillis();
    }
    public void stopStopWatchTime(){
        setPause(true);
        started = false;
        stopWatchTime = 0L;
        stopWatchTimeLabel.setText(getFormattedTime(new SimpleDateFormat("HH:mm:ss.S"), stopWatchTime));
        stopWatchTimeLabel.repaint();
    }

    public boolean isStarted() {
        return started;
    }

    public Long getStopWatchTime() { return stopWatchTime; }
    public String getFormattedTime(DateFormat dateFormat, Long stopWatchTime) {
        //Calendar calendar = Calendar.getInstance();
        //calendar.setTime(new Date(stopWatchTime));
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT0"));
        return dateFormat.format(new Date(stopWatchTime));
    }
    public String getCurrentTime() {
        return getFormattedTime(new SimpleDateFormat(DEFAULT_FORMAT), stopWatchTime);
    }

    public Long getSecToLong(Long seconds){ return seconds * 1000;}

    public boolean isPause() { return pause; }
    public void setPause(boolean pause) {
        if (!isStarted()) throw new NullPointerException("StopWatch not Stared!");
        this.pause = pause;
    }
    public boolean isSetTimerTime() { return setTimerTime; }

    private long pauseStartTime;
    public void pause(){
        if (!isPause()) pauseStartTime = System.currentTimeMillis();
        else timerTime += System.currentTimeMillis() - pauseStartTime;
        setPause(!isPause());

    }
    public boolean isInWork() { return inWork; }
    private boolean isUseClockPanel(){ return useClockPanel; }

}
