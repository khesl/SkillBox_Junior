package module_13.res.Update_13Task.CarNumberGenerator.src;

import java.io.PrintWriter;


/**
 * Created by Danya on 20.02.2016.
 */
public class Loader extends Thread
{
    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        PrintWriter writer = new PrintWriter("module_13/res/Update_13Task/CarNumberGenerator/res/numbers.txt");
        PrintWriter writer1 = new PrintWriter("module_13/res/Update_13Task/CarNumberGenerator/res/numbers1.txt");
        PrintWriter writer2 = new PrintWriter("module_13/res/Update_13Task/CarNumberGenerator/res/numbers2.txt");
        PrintWriter writer3 = new PrintWriter("module_13/res/Update_13Task/CarNumberGenerator/res/numbers3.txt");



        char letters[] = {'У', 'К', 'Е', 'Н', 'Х', 'В', 'А', 'Р', 'О', 'С', 'М', 'Т'};
        Thread th1 = new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                for(int number = 1; number < 1000; number++)
                {
                    for (int regionCode = 1; regionCode < 100; regionCode ++) {
                        for (char firstLetter : letters) {
                            for (char secondLetter : letters) {
                                for (char thirdLetter : letters) {

                                    if(builder.length() > 1_000_000) {
                                        writer.write(builder.toString());
                                        builder = new StringBuilder();
                                    }
                                    builder.append(firstLetter);

                                    if(number < 10) builder.append("00");
                                    else if(number < 100) builder.append("0");
                                    builder.append(number)
                                            .append(secondLetter)
                                            .append(thirdLetter);

                                    if(regionCode < 10) builder.append("0");
                                    builder.append(regionCode)
                                            .append('\n');
                                }
                            }
                        }
                    }
                }
                writer.write(builder.toString());
                writer.flush();
                writer.close();
            }
        });
        th1.start();
/*
        Thread th2 = new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                for(int number = 1; number < 1000; number++)
                {
                    for (int regionCode = 1; regionCode < 100; regionCode ++) {
                        for (char firstLetter : letters) {
                            for (char secondLetter : letters) {
                                for (char thirdLetter : letters) {

                                    if(builder.length() > 1_000_000)
                                    {
                                        writer1.write(builder.toString());
                                        builder = new StringBuilder();
                                    }

                                    builder.append(firstLetter);

                                    if(number < 10)
                                    {
                                        builder.append("00");
                                    }
                                    else if(number < 100)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(number)
                                            .append(secondLetter)
                                            .append(thirdLetter);
                                    if(regionCode < 10)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(regionCode)
                                            .append('\n');



                                }
                            }
                        }
                    }
                }
                writer1.write(builder.toString());
                writer1.flush();
                writer1.close();
            }
        });
        th2.start();

        Thread th3 = new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                for(int number = 1; number < 1000; number++)
                {
                    for (int regionCode = 1; regionCode < 100; regionCode ++) {
                        for (char firstLetter : letters) {
                            for (char secondLetter : letters) {
                                for (char thirdLetter : letters) {

                                    if(builder.length() > 1_000_000)
                                    {
                                        writer2.write(builder.toString());
                                        builder = new StringBuilder();
                                    }

                                    builder.append(firstLetter);

                                    if(number < 10)
                                    {
                                        builder.append("00");
                                    }
                                    else if(number < 100)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(number)
                                            .append(secondLetter)
                                            .append(thirdLetter);
                                    if(regionCode < 10)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(regionCode)
                                            .append('\n');



                                }
                            }
                        }
                    }
                }
                writer2.write(builder.toString());
                writer2.flush();
                writer2.close();
            }
        });
        th3.start();

        Thread th4 = new Thread(new Runnable() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                for(int number = 1; number < 1000; number++)
                {
                    for (int regionCode = 1; regionCode < 100; regionCode ++) {
                        for (char firstLetter : letters) {
                            for (char secondLetter : letters) {
                                for (char thirdLetter : letters) {

                                    if(builder.length() > 1_000_000)
                                    {
                                        writer3.write(builder.toString());
                                        builder = new StringBuilder();
                                    }

                                    builder.append(firstLetter);

                                    if(number < 10)
                                    {
                                        builder.append("00");
                                    }
                                    else if(number < 100)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(number)
                                            .append(secondLetter)
                                            .append(thirdLetter);
                                    if(regionCode < 10)
                                    {
                                        builder.append("0");
                                    }
                                    builder.append(regionCode)
                                            .append('\n');



                                }
                            }
                        }
                    }
                }
                writer3.write(builder.toString());
                writer3.flush();
                writer3.close();
            }
        });
        th4.start();
*/
        //скорость просто атас)

        th1.join();
      /*  th2.join();
        th3.join();
        th4.join();*/


        System.out.println((System.currentTimeMillis() - start) + " ms");
    }

}
