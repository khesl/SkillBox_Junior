package module_13.res.Update_13Task.FileEditor.src;

import javax.swing.*;

public class Louder
{
    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame();

                Form form = new Form();
                frame.setContentPane(form.getRootPanel());

                frame.setSize(800, 600);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setTitle("Text Edit");
                frame.setVisible(true);
            }
        });





    }
}
