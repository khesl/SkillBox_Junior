package module_13.res.Update_13Task.FileEditor.src;

import java.io.*;

public class XmlToTxt
{
    private final String xmlFile;
    private final String resultFile;

    public XmlToTxt(String xmlFiles, String resultFile) {
        this.xmlFile = xmlFiles;
        this.resultFile = resultFile;
    }

    public void parsingFile() {
        byte[] buffer = new byte[64 * 1024];
        OutputStream os = null;
        try {
            File result = new File(resultFile);
            os = new FileOutputStream(result);

            File source = new File(xmlFile);
            InputStream is = new FileInputStream(source);
            int readed;
            while ((readed = is.read(buffer)) > 0) {
                os.write(buffer, 0, readed);
            }
            is.close();
            os.close();
        } catch (FileNotFoundException ex) {
            System.err.println(ex.toString());
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }
}
