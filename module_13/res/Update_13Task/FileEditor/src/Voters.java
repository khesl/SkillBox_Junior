package module_13.res.Update_13Task.FileEditor.src;

public class Voters
{
    private String name;
    private String birthDay;
    public Voters(String name, String birthDay)
    {
        this.name = name;
        this.birthDay = birthDay;
    }

    public String getName() {
        return name;
    }

    public String getBirthDay() {
        return birthDay;
    }
}
