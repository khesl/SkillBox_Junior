package module_13.res.Update_13Task.FileEditor.src;

import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Form {
    private JPanel rootPanel;
    private JButton buttonOpenFile;
    private JButton buttonSaveFile;
    private JButton buttonNewFile;
    private JTextArea area;
    private JButton buttonOpenXml;
    private File file;

    public Form() {


        buttonOpenFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                chooser.showOpenDialog(rootPanel);
                file = chooser.getSelectedFile();
                try
                {
                    FileReader reader = new FileReader(file);
                    char buf[] = new char[(int) file.length()];
                    reader.read(buf);
                    area.setText(new String(buf));
                }
                catch (Exception e1)
                {
                    e1.printStackTrace();
                }
            }
        });
        buttonSaveFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileWriter writer = new FileWriter(file);
                    writer.write(area.getText());
                    writer.flush();
                    writer.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        buttonNewFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser chooser = new JFileChooser();
                if(chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
                {
                    file = chooser.getSelectedFile();
                    try {
                        FileWriter writer = new FileWriter(file);
                        writer.write(area.getText());
                        writer.flush();
                        writer.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        });
        buttonOpenXml.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String xml = "res/data-18M.xml";
                String txt = "res/result.txt";
                XmlToTxt xtt = new XmlToTxt(xml, txt);
                xtt.parsingFile();
                file = new File(txt);
                try {
                    FileReader reader = new FileReader(file);
                    char buf[] = new char[(int) file.length()];
                    reader.read(buf);
                    area.setText(new String(buf));
                }
                catch (Exception e1)
                {
                    e1.printStackTrace();
                }
//              try {
//                    parseFile(xml);
//                } catch (ParserConfigurationException e1) {
//                    e1.printStackTrace();
//                } catch (SAXException e1) {
//                    e1.printStackTrace();
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }

            }
        });
    }

    public static void parseFile(String fileName) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        BigXmlToTxt bigXmlToTxt = new BigXmlToTxt();
        parser.parse(new File(fileName), bigXmlToTxt);
    }

    public JTextArea getArea()
    {
        return area;
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }
}
