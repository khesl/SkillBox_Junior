package module_13.res.Update_13Task.FileEditor.src;

public class Visit
{
    private String station;
    private String time;

    public Visit(String station, String time)
    {
        this.station = station;
        this.time = time;
    }


    public String getStation() {
        return station;
    }

    public String getTime() {
        return time;
    }
}
