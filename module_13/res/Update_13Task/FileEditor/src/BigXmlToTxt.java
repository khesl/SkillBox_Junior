package module_13.res.Update_13Task.FileEditor.src;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.ArrayList;

public class BigXmlToTxt extends DefaultHandler
{

    private static ArrayList<Voters> voters = new ArrayList<>();
    private static ArrayList<Visit> visits = new ArrayList<>();
    public JTextArea area = new JTextArea();

    public BigXmlToTxt()
    {
        Form form = new Form();
        area = form.getArea();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("voter"))
        {
            String name = attributes.getValue("name");
            String birthDay = attributes.getValue("birthDay");
            voters.add(new Voters(name, birthDay));
        }
        else if(qName.equals("visit"))
        {
            String station = attributes.getValue("station");
            String time = attributes.getValue("time");
            visits.add(new Visit(station, time));
        }
    }

    public void printResult()
    {
        for (Voters voter : voters)
        {
            area.setText(String.format("Voter's name: %s, BirthDay: %s", voter.getName(), voter.getBirthDay()));
            System.out.println(String.format("Voter's name: %s, BirthDay: %s", voter.getName(), voter.getBirthDay()));
        }
        for (Visit visit : visits)
        {
            area.setText(String.format("Station: %s, Time: %s", visit.getStation(), visit.getTime()));
            System.out.println(String.format("Station: %s, Time: %s", visit.getStation(), visit.getTime()));
        }
    }

    public JTextArea getArea()
    {
        return area;
    }

}
