package module_13.res.old_13Task.VoteAnalyzer.src;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Created by Danya on 24.02.2016.
 */
public class Loader
{
    public static void main(String[] args) throws Exception
    {
        String fileName = "module_13/res/old_13Task/VoteAnalyzer/res/data-1572M.xml";
        // 1572 файл за 1440539 ms

        long startTime = System.currentTimeMillis();
        System.out.println("Start parsing...");
        parseFile(fileName);
        System.out.println(System.currentTimeMillis() - startTime + "ms");
    }

    private static void parseFile(String fileName) throws Exception
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        HandlerDB handler = new HandlerDB();
        parser.parse(new File (fileName), handler);
        System.out.println("here");
        handler.printResults();
    }
}