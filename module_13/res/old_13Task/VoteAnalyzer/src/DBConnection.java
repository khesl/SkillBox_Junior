package module_13.res.old_13Task.VoteAnalyzer.src;

import java.sql.*;

/**
 * Created by Danya on 24.02.2016.
 */
public class DBConnection
{
    private static Connection connection;

    private static String dbName = "learn";
    private static String dbUser = "root";
    private static String dbPass = "123456";

    private static StringBuilder builder = new StringBuilder();
    private static int bufferSize = 256_000;

    public static Connection getConnection()
    {
        if(connection == null)
        {
            try {
                connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/" + dbName +
                    "?user=" + dbUser + "&password=" + dbPass + "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
                connection.createStatement().execute("TRUNCATE TABLE voter_count");
                /*connection.createStatement().execute("DROP TABLE IF EXISTS voter_count");
                connection.createStatement().execute("CREATE TABLE voter_count(" +
                        "id INT NOT NULL AUTO_INCREMENT, " +
                        "name TINYTEXT NOT NULL, " +
                        "birthDate DATE NOT NULL, " +
                        "`count` INT NOT NULL, " +
                        "PRIMARY KEY(id))");*/
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    /**
     * индексирование ускоряет работу
     * избавление от лишних запросов, ускоряет работу
     * */
    public static void countVoter(String name, String birthDay) throws SQLException
    {
        birthDay = birthDay.replace('.', '-');
        if (builder.length() > bufferSize){
            flushCounter();
        }
        if (builder.length() == 0) {
            builder.append("INSERT INTO voter_count(name, birthDate, `count`) VALUES");
        } else {
            builder.append(',');
        }
        builder.append("('" + name + "', '" + birthDay + "', 1)");

        /*String sql = "SELECT id FROM voter_count WHERE birthDate='" + birthDay + "' AND name='" + name + "'";
        ResultSet rs = DBConnection.getConnection().createStatement().executeQuery(sql);
        if(!rs.next())
        {*/
            /*DBConnection.getConnection().createStatement()
                    .execute("INSERT INTO voter_count(name, birthDate, `count`) VALUES('" +
                            name + "', '" + birthDay + "', 1) ON DUPLICATE KEY UPDATE `count` = `count` + 1");*/
        /*}
        else {
            Integer id = rs.getInt("id");
            DBConnection.getConnection().createStatement()
                    .execute("UPDATE voter_count SET `count`=`count`+1 WHERE id=" + id);
        }*/

        // INSERT INTO table(...) VALUES(1, 2, 3), (4, 5, 6)
    }

    public static void flushCounter() throws SQLException {
        try{
        builder.append("ON DUPLICATE KEY UPDATE `count` = `count` + 1");
        DBConnection.getConnection().createStatement().execute(builder.toString());
        builder = new StringBuilder();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void printVoterCounts() throws SQLException
    {
        String sql = "SELECT name, birthDate, COUNT(*) `count` FROM voter_count GROUP BY name, birthDate HAVING `count` > 1";
        ResultSet rs = DBConnection.getConnection().createStatement().executeQuery(sql);
        while(rs.next())
        {
            System.out.println("\t" + rs.getString("name") + " (" +
                    rs.getString("birthDate") + ") - " + rs.getInt("count"));
        }
    }
}
