package module_13.Lessons_;

import module_13.res.old_13Task.VoteAnalyzer.src.HandlerDB;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Created by Danya on 24.02.2016.
 */
public class Lesson_4
{
    public static void main(String[] args) throws Exception
    {
        String fileName = "module_13/res/old_13Task/VoteAnalyzer/res/data-0.2M.xml";
        // 1572 файл за 1440539 ms
        // 18 файл за 24823ms
        // 1 файл за 6964ms
        // 0.2 файл за 3632ms

        long startTime = System.currentTimeMillis();
        System.out.println("Start parsing...");
        parseFile(fileName);
        System.out.println(System.currentTimeMillis() - startTime + "ms");
    }

    public static void parseFile(String fileName) throws Exception
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        HandlerDB handler = new HandlerDB();
        parser.parse(new File (fileName), handler);
        System.out.println("here");
        handler.printResults();
    }
}