package module_13.Lessons_.res;

import java.io.FileNotFoundException;
import java.io.IOException;

public class NumGenerator extends Thread{
    private int bufferSize = 1_000_000;
    private int minGenerateValue;
    private int maxGenerateValue;
    private String name;
    private MyFileWriter writer_;
    private boolean localWriter = false;

    public NumGenerator(int minGenerateValue, int maxGenerateValue, String name){
        if (minGenerateValue > maxGenerateValue) throw new IllegalArgumentException("Min value must be less than Max value.");
        if (minGenerateValue == 0) minGenerateValue = 1;
        this.minGenerateValue = minGenerateValue;
        this.maxGenerateValue = maxGenerateValue;
        this.name = name;
    }
    public NumGenerator(int minGenerateValue, int maxGenerateValue, int bufferSize, String name, MyFileWriter writer){
        this(minGenerateValue, maxGenerateValue, name);
        this.bufferSize = bufferSize;
        writer_ = writer;
    }
    public NumGenerator(int minGenerateValue, int maxGenerateValue, int bufferSize, String name, String filePath) throws FileNotFoundException {
        this(minGenerateValue, maxGenerateValue, name);
        this.bufferSize = bufferSize;
        writer_ = new MyFileWriter(filePath);
        localWriter = true;
    }

    public NumGenerator(int minGenerateValue, int maxGenerateValue, String name, String filePath) throws FileNotFoundException {
        this(minGenerateValue, maxGenerateValue, name);
        writer_ = new MyFileWriter(filePath);
        localWriter = true;
    }

    @Override
    public void run(){
        while(!interrupted()) {
            System.out.println(name + " start." + "from '" + minGenerateValue + "' to '" + maxGenerateValue + "'");
            long start = System.currentTimeMillis();

            char letters[] = {'У', 'К', 'Е', 'Н', 'Х', 'В', 'А', 'Р', 'О', 'С', 'М', 'Т'};
            StringBuilder builder = new StringBuilder();
            for(int number = minGenerateValue; number < maxGenerateValue; number++){
                //int regionCode = 199;
                for (int regionCode = 1; regionCode < 110; regionCode++)
                    for (char firstLetter : letters) {
                        for (char secondLetter : letters) {
                            for (char thirdLetter : letters) {
                                if (builder.length() > bufferSize){
                                    writer_.write(builder);
                                    builder = new StringBuilder();
                                }

                                builder.append(firstLetter);
                                if (number < 10) builder.append("00");
                                else if (number < 100) builder.append("0");
                                builder.append(number)
                                        .append(secondLetter)
                                        .append(thirdLetter);
                                if (regionCode < 10) builder.append("0");
                                builder.append(regionCode)
                                        .append('\n');
                            }
                        }
                    }
            }
            writer_.write(builder);
            if (localWriter) writer_.flushWriter();

            System.out.println(name + " is done. " + (System.currentTimeMillis() - start) + " ms");
            break;
        }
    }
}